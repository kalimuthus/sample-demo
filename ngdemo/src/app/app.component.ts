import { getLocaleDateFormat } from '@angular/common';
import { Component } from '@angular/core';
import { UsersService } from './users.service';    

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  sidemenus: any = [
    { path: '/',          name: 'Home'},
    { path: '/dashboard', name : 'Dashbaord' },
    { path: '/login',     name : 'Login' },
    { path: '/setting',     name : '' }
  ];


  title = 'demo';
  constructor(private user:UsersService){
    this.user.getData().subscribe(data=>{
      console.warn(data)
    })
  }
}
