import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  dashboard : any;

  constructor(dataService: DataService) { 
    dataService.getData().then((data: any) => {
      this.dashboard = data.dashboard;
    });
  }

  ngOnInit(): void {
  }
  

}
