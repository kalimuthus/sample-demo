# Demo

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



Step 1. (Add two component - home/dashboard) 
1. ng generate component home
2. ng generate component dashboard
3. clean Home page (Remove the body/content, execpt the header). 
4. Added navigation/routing(https://angular.io/start/start-routing)

Step 2. (Install angular material)
1. ng add @angular/material (npm install @angular/material should also work).

Step 3. (Installed Toolbar - https://material.angular.io/components/toolbar/overview )

Step 4. Add Side Menu and Main content (https://material.angular.io/components/sidenav/overview)

Step 5. Create Dashboard components with local data as variable. 

Step 6. Create Service to get the data from a file(https://angular.io/start/start-data#use-httpclient-in-the-appmodule).
ng generate service data

Step 7. Fillin all the data and complete the dashbaord. 

Step 8. Added Side Menu with Navigation (https://material.angular.io/components/list/overview). 

Step 9. Get data from remote server(Create a node Server).

Step 10. Keep refining with more data. 


Questions
Dependency Injection
Constructor vs ngOnInit - Life cycle
Promise vs Observable

